FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI_append_t4240rdb-64b = " file://dts/t4240rdb-usdpaa-enea.dts"
SRC_URI_append_p2041rdb = " file://dts/p2041rdb-usdpaa-enea.dts"
SRC_URI_append_p3041ds = " file://dts/p3041ds-usdpaa-enea.dts"

KERNEL_DEVICETREE_append_t4240rdb-64b = " t4240rdb-usdpaa-enea.dtb"
KERNEL_DEVICETREE_append_p2041rdb = " p2041rdb-usdpaa-enea.dtb"
KERNEL_DEVICETREE_append_p3041ds = " p3041ds-usdpaa-enea.dtb"

ENEA_KERNEL_FRAGMENTS_append = " \
    cfg/uio_fsl_srio.cfg \
    cfg/rcu_boost.cfg \
    cfg/rcu_nocb.cfg \
    cfg/debug_off.cfg \
    cfg/profiling_off.cfg \
    cfg/rt_lld.cfg \
    cfg/nohz.cfg \
    cfg/hotplug_cpu.cfg \
    cfg/ppc/erratum_off.cfg \
    "

do_configure_prepend_t4240rdb-64b() {
    cp ${WORKDIR}/dts/t4240rdb-usdpaa-enea.dts ${S}/arch/powerpc/boot/dts/
}

do_configure_prepend_p2041rdb() {
    cp ${WORKDIR}/dts/p2041rdb-usdpaa-enea.dts ${S}/arch/powerpc/boot/dts/
}

do_configure_prepend_p3041ds() {
    cp ${WORKDIR}/dts/p3041ds-usdpaa-enea.dts ${S}/arch/powerpc/boot/dts/
}
