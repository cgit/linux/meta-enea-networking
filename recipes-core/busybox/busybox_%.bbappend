FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI += " \
           file://arp.cfg              \
           file://ip_extra.cfg         \
           file://nc_extra.cfg         \
           file://netstat_extra.cfg    \
           file://traceroute.cfg       \
           "
