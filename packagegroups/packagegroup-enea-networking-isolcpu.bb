SUMMARY = "Packages needed for cpu isolation support"
DESCRIPTION = "This package group includes packages required by cpu isolation"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LICENSE;md5=4d92cd373abda3937c2bc47fbc49d690"

inherit packagegroup

PACKAGES = "${PN}"

RDEPENDS_${PN} = " \
    partrt \
    "

RRECOMMENDS_${PN} = ""
