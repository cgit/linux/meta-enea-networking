SUMMARY = "Packages specific to the Enea Linux Networking Profile"
DESCRIPTION = "This package group includes packages and packagegroups required for the Enea Linux \
               Networking Profile."

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LICENSE;md5=4d92cd373abda3937c2bc47fbc49d690"


inherit packagegroup

PACKAGES = "${PN}"

RDEPENDS_${PN} = " \
    packagegroup-enea-networking-isolcpu \
    packagegroup-enea-networking-usdpaa \
    "

RRECOMMENDS_${PN} = ""
