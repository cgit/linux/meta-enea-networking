SUMMARY = "Packages needed for Freescale USDPAA support"
DESCRIPTION = "This package group includes userspace packages required by USDPAA."

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LICENSE;md5=4d92cd373abda3937c2bc47fbc49d690"

inherit packagegroup

USDPAA_PKGS = " \
    eth-config \
    flib \
    fmc \
    fmlib \
    usdpaa \
    usdpaa-apps \
    "

PACKAGES = "${PN}"

RDEPENDS_${PN} = " ${USDPAA_PKGS} \
    "

RRECOMMENDS_${PN} = ""
